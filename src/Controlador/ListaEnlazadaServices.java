/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

/**
 *
 * @author LENOVO
 */
public class ListaEnlazadaServices <E>{
    private ListaEnlazada<E> lista;
    
    public ListaEnlazada<E> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<E> lista) {
        this.lista = lista;
    }
    
    public ListaEnlazadaServices() {
        this.lista = new ListaEnlazada<>();
    }
    
    public void insertarAlInicio(E dato) {       
            lista.insertarCabecera(dato);
       
    }
    public void insertarAlFinal(E dato) {
            lista.insertar(dato, lista.getSize() - 1);
    }
    public void insertar(E dato, Integer pos) {       
            lista.insertar(dato, pos);
    }
    
    public Integer getSize() {
        return lista.getSize();
    }
    
    public E obtenerDato(Integer pos) {
        try {
            return lista.obtenerDato(pos);
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }
    
    public void eliminarCabecera() {
            lista.eliminarDato(0);
    }
    
    public void eliminarUltimo() {
            lista.eliminarDato(lista.getSize() - 1);
    }
    
    public E eliminarPosicion(Integer pos) {
            return lista.eliminarDato(pos);
    }
    
    public void modificarDatoPosicion(Integer pos, E dato) {
            lista.modificarDato(pos, dato);
    }
    
    public void limpiarLista() {
        lista.vaciar();        
    }
    
    public Boolean estaVacia(){
        return lista.estaVacia();
    }
}
