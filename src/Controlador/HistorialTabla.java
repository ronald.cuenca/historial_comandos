/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.Historial;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author LENOVO
 */
public class HistorialTabla extends AbstractTableModel{
    private Pila<Historial> pila;

    public Pila<Historial> getPila() {
        return pila;
    }

    public void setPila(Pila<Historial> pila) {
        this.pila = pila;
    }
    
    @Override
    public int getRowCount() {
        return pila.getSize();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column) {
            case 0: return "                Fecha";
            case 1: return "            Comando";
            default: return null;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Historial historial = (Historial) pila.obtenerDato(rowIndex);
        switch(columnIndex) {
            case 0: return historial.getFecha();
            case 1: return historial.getComando();
            default: return null;
        }
    }
}
