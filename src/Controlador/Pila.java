/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.Historial;

/**
 *
 * @author LENOVO
 */
public class Pila <E> extends ListaEnlazada<E>{
    private Integer tope;

    public Pila(Integer tope) {
        this.tope = tope;
    }
    
    public Pila() {
        this.tope = 0;
    }
    
    public Boolean estaLleno(){
        if (tope==0) 
            return false;
        else if (tope == getSize().intValue()) 
            return true;
        else
            return false;
    }
    
    public void push(E dato){
        if (!estaLleno()) {
            insertarCabecera(dato);
        }else{
            System.out.println("La pila esta Llena");
        }
    }
    
    public void pop(Integer pos){
        if (!estaVacia()) {
            if (pos >= 0 && pos < getSize()) {
                for (int i = 0; i <= pos; i++) {
                    eliminarDato2(getSize());
                }
        }else{
            System.out.println("La pila esta Vacia");
        }
    }else{
            System.out.println("La pila esta Vacia");
        }
    }
    
    public Integer getTope(){
        return tope;
    }
}
